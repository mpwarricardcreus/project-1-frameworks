Introducción:
El proyecto es un "Blog" que tiene artículos, autores y comentarios, también tiene unos eventos que representan la gestión de la caché del contenido.
El código está compuesto por:
ArticleBundle: Bundle que permite generar artículos de un blog con sus respectivos campos (título, descripción y autor) El autor es un ManyToOne con la entidad Author que está en el AuthorBundle. Tiene una relación OneToMany con la Entidad Comments. Se pueden listar, editar, crear y borrar. Cuando se crea un artículo se debe seleccionar a qué autor (de los creados) se asigna.
AuthorBundle: Bundle que permite crear autores, editarlos, borrarlos. Sus campos son name, email y nickname. Tiene una relación OneToMany con los artículos. Hay una vista que lista todos los artículos de ese autor.
CacheBundle: bundle creado para gestionar los eventos de cache de los artículos, se ejecutan cuando se crea un artículo, se borra o se actualiza, la idea (no implementada) sería que cacheara el contenido inmediatamanete cuando se ejecuta alguna acción en el artículo.
CommentsBundle: Bundle para añadir comentarios a los artículos, sólo se pueden crear, y cuando se crean pide a qué articulo asocias el comentario. Tiene una relación ManyToOne con la entidad Article. Sus campos son idArticle, author y commentText.
Actualmente hay una vista para crear comentarios pero debería estar integrada con la vista de artículos. A la vez, cuando se ejecuta la vista de artículos muestra todos los comentarios que tiene ese artículo, y tiene el enlace para añadir uno nuevo.
También tiene un evento declarado para cuando se crea un comentario pero nadie lo recoge actualmente.



Versión 2:
Se ha eliminado toda la lógica de negocio del controlador, para ello se han creado diferentes servicios que gestionan esa lógica.
Se ha refractorizado el código ganando en semática y calidad.
Se han aplicado principios de SOLID.
Se han mejorado las plantillas de twig con estilos básicos.
Se ha mantenido la estructura de 4 bundles, el proyecto está compuesto básicamente por (Article, Author y Comments) pero el bundle
 de caché est´á creado para verificar los eventos y ver que se ejecutan correctamente.
Se han mejorado las validaciones de los campos tanto en el front como en el back.
Los caso de errores se han tratado de tal forma que el usuario pueda seguir navegando sin que se cuelgue la página.
Se han añadido comentarios a las clases y métodos importantes, para facilitar la lectura de terceros y la estructura de la aplicación.

En conclusión, la diferencia respecto la primera versión es muy notable, ahora es fácil de leer e identificar trozos de código,
Las variables dan mucha información y el código es limpio respecto la primera versión, su funcionamiento continua siendo el mismo e incluso
se ha añadido alguna funcionalidad más y mejor usabilidad.
El cambio de algo existente mal hecho a algo "estable y límpio" es difícil, pero a la larga se valora mucho.
Se intentaron añadir casos de test pero tube problemas con la versión de symfony y lo aparté después de muchas peleas.
