<?php


namespace ArticleBundle\Controller;
use ArticleBundle\Entity\Article;
use ArticleBundle\Event\ArticleCreatedEvent;
use ArticleBundle\Event\ArticleEditedEvent;
use ArticleBundle\Event\ArticleRemovedEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use \ArticleBundle\Form\Type\ArticleType;

class ArticleController extends Controller{

    /**
     * Create Article controller function
     * @Route("/newarticle", name="newArticle")
     * @Template
     */
    public function createAction(Request $request)
    {
        $createArticleService = $this->get('app.create_article');
        $createArticle = $createArticleService->create($request);

        if (array_key_exists('id', $createArticle)){
            return $this->redirectToRoute('viewAuthor',$createArticle);
        }
        return $createArticle;
    }

    /**
     * List Articles controller function
     * @Route("/listarticles", name="listArticles")
     * @Template
     */
    public function listAction(){
        
        $listAllArticlesService = $this->get('app.list_articles');
        $allArticles = $listAllArticlesService->listAll();
        
        return $allArticles;
    }
    
    /**
     * Edit articles controller function
     * @Route("/editarticle/{id}", name="editArticle")
     * @Template
     */
    public function editAction(Request $request, $id){
        $editArticleService = $this->get('app.edit_article');
        $editArticle = $editArticleService->edit($request,$id);

        if ($editArticle === null) {
            return $this->redirectToRoute('listArticles');
        }
        return $editArticle;
        
    }


    /**
     * View article controller function
     * @Route("/viewarticle/{id}", name="viewArticle")
     * @Template
     */
    public function viewAction(Request $request,$id)
    {
        $viewArticleService = $this->get('app.view_article');
        $articleViewer = $viewArticleService->view($request, $id);
        
        if ($articleViewer === null) {
            return $this->redirectToRoute('listArticles');
        }
        return array('pack' => $articleViewer);
    }

    /**
     * Remove article controller function
     * @Route("/removearticle/{id}", name="removeArticle")
     * @Template
     */
    public function removeAction(Request $request, $id){

        $removeArticleService = $this->get('app.remove_article');
        $removeArticle = $removeArticleService->remove($request, $id);
        if (!$removeArticle) {
            return array('removed' => 'The article is not removed'); 
        }
        return array('removed' => 'removed correctly');
    }
    
}