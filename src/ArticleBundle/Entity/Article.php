<?php
/**
 * Created by PhpStorm.
 * User: ricard
 * Date: 27/2/17
 * Time: 18:36
 */

namespace ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity  */
class Article
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $description;


    /**
     * @ORM\ManyToOne(targetEntity="AuthorBundle\Entity\Author", inversedBy="article")
     * @ORM\JoinColumn(name="id_author", referencedColumnName="id")
     *
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity="CommentsBundle\Entity\Comments", mappedBy="article", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="id_comment", referencedColumnName="id")
     *
     */
    private $comments;
    
    
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param mixed $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }
    
}