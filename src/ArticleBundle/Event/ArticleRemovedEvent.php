<?php
/**
 * Created by PhpStorm.
 * User: ricard
 * Date: 5/3/17
 * Time: 19:41
 */

namespace ArticleBundle\Event;


use ArticleBundle\Entity\Article;
use Symfony\Component\EventDispatcher\Event;

class ArticleRemovedEvent extends Event
{
    private $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    /**
     * @return Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param Article $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }
}