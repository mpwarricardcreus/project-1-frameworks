<?php

namespace ArticleBundle\Form\Type;

use AuthorBundle\AuthorBundle;
use AuthorBundle\Entity\Author;
use AuthorBundle\Form\Type\AuthorType;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ArticleType extends AbstractType
{

    public function buildForm (FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'required' => true,
            ))
            ->add('description', TextType::class, array(
                'required'  => true,
            ))
            ->add('author', EntityType::class, array(
                'class'   => 'AuthorBundle:Author',
                'choice_label' => 'name',
                'multiple' => false,
                'required' => true,
            ))
            ->add('save',SubmitType::class, array('label' => 'Create Article'));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ArticleBundle\Model\ArticleModel'
        ));
    }
    
}