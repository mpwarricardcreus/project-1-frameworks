<?php

namespace ArticleBundle\Services;


use ArticleBundle\Entity\Article;
use ArticleBundle\Model\ArticleModel;

/**
 * This class contains the common functions of article services
 * Class ArticleUtils
 * @package ArticleBundle\Services
 */
class ArticleUtils
{
    /**
     * Transfroms the Article Model object to Article Entity object
     * @param ArticleModel $articleModel
     * @return Article
     */
    public static function generateNewArticleEntity(ArticleModel $articleModel) : Article{

        $articleEntity = new Article();
        $articleEntity->setId($articleModel->getId());
        $articleEntity->setAuthor($articleModel->getAuthor());
        $articleEntity->setComments($articleModel->getComments());
        $articleEntity->setDescription($articleModel->getDescription());
        $articleEntity->setTitle($articleModel->getTitle());

        return $articleEntity;
    }

    /**
     * This function generates the copy of edited values from article model on Entity Article
     * @param ArticleModel $articleModel
     * @param Article $articleEntityLoaded
     * @return Article
     */
    public static function generateCopyArticleEntity(ArticleModel $articleModel, Article $articleEntityLoaded) : Article
    {

        $articleEntity = $articleEntityLoaded;
        $articleEntity->setId($articleModel->getId());
        $articleEntity->setAuthor($articleModel->getAuthor());
        $articleEntity->setComments($articleModel->getComments());
        $articleEntity->setDescription($articleModel->getDescription());
        $articleEntity->setTitle($articleModel->getTitle());

        return $articleEntity;
    }

    /**
     * Transfroms the Article Entity to Article Model object
     * @param Article $articleEntityLoaded
     * @return ArticleModel
     */
    public static function generateArticleModel(Article $articleEntityLoaded): ArticleModel {
        $articleModel = new ArticleModel();
        if ($articleEntityLoaded !== null) {
            $articleModel->setAuthor($articleEntityLoaded->getAuthor());
            $articleModel->setTitle($articleEntityLoaded->getTitle());
            $articleModel->setDescription($articleEntityLoaded->getDescription());
            $articleModel->setComments($articleEntityLoaded->getComments());
            $articleModel->setId($articleEntityLoaded->getId());
        }

        return $articleModel;
    }

}