<?php


namespace ArticleBundle\Services;


use ArticleBundle\Event\ArticleCreatedEvent;
use ArticleBundle\Form\Type\ArticleType;
use ArticleBundle\Model\ArticleModel;
use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcher;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service Class to create, validate and save articles.
 * Generates Event listeners
 * Class CreateArticle
 * @package ArticleBundle\Services
 */
class CreateArticle
{

    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var FormFactoryInterface */
    private $form;
    /** @var  TraceableEventDispatcherInterface */
    private $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $form, TraceableEventDispatcher $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->form = $form;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Return empty new article form or validate and save the new article added
     * @param $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function create(Request $request) {
        $articleModel = new ArticleModel();
        $form = $this->form->create(ArticleType::class,$articleModel);

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $articleEntity = ArticleUtils::generateNewArticleEntity($form->getData());
            $isSaved = $this->saveArticle($articleEntity);
            if ($isSaved) {
                $this->generateCreateArticleEvent($articleEntity);
                return array('id' => $articleEntity->getId());
            }
        }

        return array('form' => $form->createView());
    }


    /**
     * Save article on data base
     * @param $articleEntity
     * @return bool
     */
    private function saveArticle($articleEntity): bool
    {
        try {
            $this->entityManager->persist($articleEntity);
            $this->entityManager->flush();
        } catch(DatabaseObjectExistsException $e){
            echo "Error saving article: " . $e;
            return false;
        }
        return true;
        
    }

    /**
     * Generates Event listener 'article_created'
     * @param $articleEntity
     */
    private function generateCreateArticleEvent($articleEntity)
    {
        $this->eventDispatcher->dispatch('article_created', new ArticleCreatedEvent($articleEntity));
    }
}