<?php


namespace ArticleBundle\Services;


use ArticleBundle\Entity\Article;
use ArticleBundle\Event\ArticleEditedEvent;
use ArticleBundle\Form\Type\ArticleType;
use ArticleBundle\Model\ArticleModel;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\CssSelector\Parser\Reader;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcher;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service class to edit articles and update it
 * Generates Event listeners
 * Class EditArticle
 * @package ArticleBundle\Services
 */
class EditArticle
{

    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var FormFactoryInterface */
    private $form;
    /** @var  TraceableEventDispatcherInterface */
    private $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $form, TraceableEventDispatcher $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->form = $form;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Return article form with data or validate and save the new article edited
     * @param $request
     * @param $id
     * @return \Symfony\Component\Form\FormInterface
     */
    public function edit(Request $request, $id)
    {

        $articleEntityLoaded = $this->recoverArticleById($id);

        $articleModel = ArticleUtils::generateArticleModel($articleEntityLoaded);

        $form = $this->form->create(ArticleType::class, $articleModel);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $articleEntity = ArticleUtils::generateCopyArticleEntity($form->getData(), $articleEntityLoaded);
            $isSaved = $this->saveArticle($articleEntity);
            if ($isSaved) {
                $this->generateCreateArticleEvent($articleEntity);
                return array('id' => $articleEntity->getId());
            }
        }

        return array('form' => $form->createView());
    }

    /**
     * @return bool
     */
    private function saveArticle() :bool
    {
        try {
            $this->entityManager->flush();
        } catch (DatabaseObjectExistsException $e) {
            echo "Error saving article: " . $e;
            return false;
        }
        return true;
    }

    /**
     * @param $articleEntity
     */
    private function generateCreateArticleEvent(Article $articleEntity)
    {
        $this->eventDispatcher->dispatch('article_edited', new ArticleEditedEvent($articleEntity));
    }

    /**
     * Find article on data base
     * @param $id
     * @return Article
     */
    private function recoverArticleById(string $id) : Article
    {
        try {
            $repository = $this->entityManager->getRepository('ArticleBundle\Entity\Article');
            $articleEntityLoaded = $repository->find($id);
        } catch (PDOException $e) {
            echo "Error getting article " . $e;
            return null;
        }

        return $articleEntityLoaded;
    }

}