<?php


namespace ArticleBundle\Services;


use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service class list all articles from all authors
 * Class ListArticles
 * @package ArticleBundle\Services
 */
class ListArticles
{

    /** @var EntityManagerInterface */
    private $entityManager;
    
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Return all articles
     * @return array
     */
    public function listAll(): array {

        $allArticles = $this->recoverAllArticles();
        return array('articles' => $allArticles);
    }

    /**
     * find all articles on data base 
     * @return array
     */
    private function recoverAllArticles() : array
    {
        try {
            $repository = $this->entityManager->getRepository('ArticleBundle\Entity\Article');
            $allArticles = $repository->findAll();
        } catch (DatabaseObjectExistsException $e){
            echo "Error find articles: " . $e;
            return array();
        }
        
        return $allArticles;
    }

}