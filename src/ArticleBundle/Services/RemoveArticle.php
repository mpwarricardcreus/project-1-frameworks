<?php


namespace ArticleBundle\Services;


use ArticleBundle\Event\ArticleRemovedEvent;
use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcher;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service class to remove article
 * Class RemoveArticle
 * @package ArticleBundle\Services
 */
class RemoveArticle
{
    const ARTICLE_BUNDLE_ENTITY_ARTICLE = 'ArticleBundle\Entity\Article';

    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var  TraceableEventDispatcherInterface */
    private $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager, TraceableEventDispatcher $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Find the article and remove it
     * @param Request $request
     * @param string $id
     * @return bool
     */
    public function remove(Request $request, string $id): bool{

        $articleEntity = $this->recoverArticleEntity($id);
        $isRemoved = $this->removeArticleEntity($articleEntity);
        if ($isRemoved) {
            $this->eventDispatcher->dispatch('article_removed',new ArticleRemovedEvent($articleEntity));
            return true;
        }
        return false;

    }

    /**
     * @param string $id
     * @return object
     */
    private function recoverArticleEntity(string $id)
    {
        try {
            $repository = $this->entityManager->getRepository(self::ARTICLE_BUNDLE_ENTITY_ARTICLE);
            $articleEntity = $repository->find($id);
        } catch (DatabaseObjectExistsException $e){
            echo "Error getting articleview " . $e;
            return null;
        }

        return $articleEntity;
    }

    /**
     * @param $articleEntity
     * @return bool
     */
    private function removeArticleEntity($articleEntity): bool
    {
        try {
            $this->entityManager->remove($articleEntity);
            $this->entityManager->flush();
        }catch (DatabaseObjectExistsException $e){
            echo "Error getting articleview " . $e;
            return false;
        }
        return true;
    }
}