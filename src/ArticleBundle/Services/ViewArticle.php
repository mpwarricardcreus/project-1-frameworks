<?php


namespace ArticleBundle\Services;


use ArticleBundle\Entity\Article;
use ArticleBundle\Model\ArticleModel;
use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ViewArticle
 * @package ArticleBundle\Services
 */
class ViewArticle
{
    const ARTICLE_BUNDLE_ENTITY_ARTICLE = 'ArticleBundle\Entity\Article';

    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var FormFactoryInterface */
    private $form;

    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $form)
    {
        $this->entityManager = $entityManager;
        $this->form = $form;
    }
    
    public function view(Request $request, string $id){

        $articleEntity = $this->recoverArticleEntity($id);
        $articleModel = ArticleUtils::generateArticleModel($articleEntity);
        if ($articleModel !== null) {
            $allComments = $articleEntity->getComments();
            return array($articleModel, $allComments);
        }
        
        return null;
        
    }

    /**
     * @param string $id
     * @return object
     */
    private function recoverArticleEntity(string $id)
    {
        try {
            $repository = $this->entityManager->getRepository(self::ARTICLE_BUNDLE_ENTITY_ARTICLE);
            $articleEntity = $repository->find($id);
        } catch (DatabaseObjectExistsException $e){
            echo "Error getting articleview " . $e;
            return null;
        }
        
        return $articleEntity;
    }

   
}