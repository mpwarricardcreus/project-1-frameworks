<?php

namespace AuthorBundle\Controller;

use \Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AuthorController extends Controller{


    /**
     * Create author function calls CreateAuthorService.
     * @Route("/newauthor", name="newAuthor")
     * @Template
     */
    public function createAction(Request $request)
    {
        $createAuthorService = $this->get('app.create_author');
        $createAuthor = $createAuthorService->create($request);
        if (array_key_exists('id', $createAuthor)){
            return $this->redirectToRoute('viewAuthor',$createAuthor);
        }
        return $createAuthor;

    }

    /**
     * View Author function calls ViewAuthorService
     * @Route("/viewauthor/{id}", name="viewAuthor")
     * @Template
     */
    public function viewAction(Request $request, $id)
    {
        $viewAuthorService = $this->get('app.view_author');
        $viewAuthor = $viewAuthorService->view($request, $id);

        if($viewAuthor !== null) {
            return $viewAuthor;
        }
        return $this->redirectToRoute('listArticles');
    }

    /**
     * Remove author function calls RemoveAuthorService
     * @Route("/removeauthor/{id}", name="removeAuthor")
     * @Template
     */
    public function removeAction(Request $request, $id){

        $removeAuthorService = $this->get('app.remove_author');
        $isRemoved = $removeAuthorService->remove($request, $id);
        if($isRemoved) {
            return array('removed' => 'Author and his articles have been deleted');
        }

        return array('removed' => 'Author and his articles haven\'t been deleted');
    }


    /**
     * List all articles by author calls ListArticlesByAuthorService
     * @Route("/author/{id}/articles", name="listArticleAuthor")
     * @Template
     */
    public function listarticleAction(Request $request, $id){

        $listArticlesByAuthorService = $this->get('app.list_author_article');
        $listArticlesAuthor = $listArticlesByAuthorService->listArticlesAuthor($request, $id);

        if ($listArticlesAuthor === null) {
            return $this->redirectToRoute('listArticles');
        }
        return $listArticlesAuthor;

    }

    /**
     * Edit author function calls EditAuthorService
     * @Route("/editauthor/{id}", name="editAuthor")
     * @Template
     */
    public function editAction(Request $request,$id){

        $editAuthorService = $this->get('app.edit_author');
        $editAuthor = $editAuthorService->edit($request, $id);

        if (array_key_exists('id', $editAuthor)){
            return $this->redirectToRoute('viewAuthor',$editAuthor);
        }
        return $editAuthor;

    }
}