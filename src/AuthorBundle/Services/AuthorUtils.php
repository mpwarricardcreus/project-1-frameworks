<?php


namespace AuthorBundle\Services;


use AuthorBundle\Entity\Author;
use AuthorBundle\Model\AuthorModel;

/**
 * This class contains the common functions of author services
 * Class AuthorUtils
 * @package AuthorBundle\Services
 */
class AuthorUtils
{

    /**
     * This function generates the copy of edited values from author model on Entity Author
     * @param AuthorModel $authorModel
     * @param Author $authorEntity
     * @return Author
     */
    public static function generateCopyAuthorEntity(AuthorModel $authorModel, Author $authorEntity): Author
    {
        $authorEntity = $authorEntity;
        if ($authorModel !== null) {
            $authorEntity->setId($authorModel->getId());
            $authorEntity->setArticle($authorModel->getArticle());
            $authorEntity->setEmail($authorModel->getEmail());
            $authorEntity->setName($authorModel->getName());
            $authorEntity->setNickname($authorModel->getNickname());
        }

        return $authorEntity;
    }

    /**
     *Transfroms the Author Entity to Author Model object
     * @param Author $authorEntity
     * @return AuthorModel
     */
    public static function generateAuthorModel(Author $authorEntity): AuthorModel
    {
        $authorModel = new AuthorModel();
        $authorModel->setId($authorEntity->getId());
        $authorModel->setNickname($authorEntity->getNickname());
        $authorModel->setName($authorEntity->getName());
        $authorModel->setEmail($authorEntity->getEmail());
        $authorModel->setArticle($authorEntity->getArticle());

        return $authorModel;
    }

    /**
     * Transfroms the Author Model object to Author Entity object
     * @param AuthorModel $authorModel
     * @return Author
     */
    public static function generateNewAuthorEntity(AuthorModel $authorModel): Author
    {
        $authorEntity = new Author();
        if ($authorModel !== null) {

            $authorEntity->setId($authorModel->getId());
            $authorEntity->setArticle($authorModel->getArticle());
            $authorEntity->setEmail($authorModel->getEmail());
            $authorEntity->setName($authorModel->getName());
            $authorEntity->setNickname($authorModel->getNickname());
        }

        return $authorEntity;
    }
}