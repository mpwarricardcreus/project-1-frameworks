<?php


namespace AuthorBundle\Services;


use AuthorBundle\Entity\Author;
use AuthorBundle\Form\Type\AuthorType;
use AuthorBundle\Model\AuthorModel;
use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service class to create authors
 * Class CreateAuthor
 * @package AuthorBundle\Services
 */
class CreateAuthor
{
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var FormFactoryInterface */
    private $form;

    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $form)
    {
        $this->entityManager = $entityManager;
        $this->form = $form;
    }

    /**
     * Service to create new Authors, validate/create forms and save data
     * @param Request $request
     * @return array
     */
    public function create(Request $request){

        $authorModel = new AuthorModel();
        
        $form = $this->form->create(AuthorType::class,$authorModel);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $authorEntity = AuthorUtils::generateNewAuthorEntity($form->getData());
            $isSaved = $this->saveAuthor($authorEntity);
            if ($isSaved) {
                return array('id' => $authorEntity->__toString());
            }
            
        }

        return array('form' => $form->createView());
    }


    /**
     * Save the author entity to data base
     * @param $authorEntity
     * @return bool
     */
    private function saveAuthor($authorEntity): bool
    {
        try {
            $this->entityManager->persist($authorEntity);
            $this->entityManager->flush();
        } catch (DatabaseObjectExistsException $e) {
            echo "Error getting author: " . $e;
            return false;
        }
        return true;
        
    }

}