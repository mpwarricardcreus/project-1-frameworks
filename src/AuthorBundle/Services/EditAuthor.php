<?php


namespace AuthorBundle\Services;


use AuthorBundle\Entity\Author;
use AuthorBundle\Form\Type\AuthorType;
use AuthorBundle\Model\AuthorModel;
use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service class to edit authors
 * Class EditAuthor
 * @package AuthorBundle\Services
 */
class EditAuthor
{
    const AUTHOR_BUNDLE_ENTITY_AUTHOR = 'AuthorBundle\Entity\Author';

    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var FormFactoryInterface */
    private $form;

    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $form)
    {
        $this->entityManager = $entityManager;
        $this->form = $form;
    }

    /**
     * This function edit data from authors, generates the form, validate and save on data base
     * @param Request $request
     * @param string $id
     * @return array
     */
    public function edit(Request $request, string $id){

        $authorEntity = $this->recoverAuthorByid($id);
        $authorModel = AuthorUtils::generateAuthorModel($authorEntity);

        $form = $this->form->create(AuthorType::class,$authorModel);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $author = AuthorUtils::generateCopyAuthorEntity($form->getData(), $authorEntity);
            $isSaved = $this->saveAuthor($author);
            if($isSaved) {
                return array('id' => $author->__toString());
            }
            
        }

        return array('form' => $form->createView());
    }

   
    /**
     * Recover the author by the id
     * @param string $id
     * @return object
     */
    private function recoverAuthorByid(string $id)
    {
        $repository = $this->entityManager->getRepository(self::AUTHOR_BUNDLE_ENTITY_AUTHOR);
        $author = $repository->find($id);
        return $author;
    }

    /**
     * Save the author entity to data base
     * @param $authorEntity
     * @return bool
     */
    private function saveAuthor($authorEntity): bool
    {
        try {
            $this->entityManager->persist($authorEntity);
            $this->entityManager->flush();
        } catch (DatabaseObjectExistsException $e) {
            echo "Error getting author: " . $e;
            return false;
        }
        return true;

    }

}