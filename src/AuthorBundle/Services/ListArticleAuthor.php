<?php


namespace AuthorBundle\Services;


use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service class to list all articles from one author
 * Class ListArticleAuthor
 * @package AuthorBundle\Services
 */
class ListArticleAuthor
{
    const AUTHOR_BUNDLE_ENTITY_AUTHOR = 'AuthorBundle\Entity\Author';

    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var FormFactoryInterface */
    private $form;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * This function find all articles from author and return array filled with it
     * @param Request $request
     * @param string $id
     * @return array|null
     */
    public function listArticlesAuthor(Request $request, string $id){

        $authorEntity = $this->findAuthorById($id);
        if($authorEntity !== null) {
            $articlesAuthor = $authorEntity->getArticle();
            return array('articles' => $articlesAuthor);
        }
        
        return null;
        
    }

    /**
     * Find the author by the id
     * @param string $id
     * @return mixed
     */
    private function findAuthorById(string $id)
    {
        try{
            $repository = $this->entityManager->getRepository(self::AUTHOR_BUNDLE_ENTITY_AUTHOR);
            $authorEntity = $repository->find($id);
        }catch(DatabaseObjectExistsException $e) {
            echo "Error getting author: " . $e;
            return null;
        }
        return $authorEntity;
    }


}