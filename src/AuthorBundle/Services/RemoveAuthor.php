<?php

namespace AuthorBundle\Services;


use AuthorBundle\Entity\Author;
use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service class to remove author and all articles from him
 * Class RemoveAuthor
 * @package AuthorBundle\Services
 */
class RemoveAuthor
{
    const AUTHOR_BUNDLE_ENTITY_AUTHOR = 'AuthorBundle\Entity\Author';

    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Find the author by id and remove from data base
     * @param Request $request
     * @param string $id
     * @return bool
     */
    public function remove(Request $request, string $id)
    {

        $authorEntity = $this->recoverAuthorEntity($id);
        if ($authorEntity === null) {
            return false;
        }
        $isRemoved = $this->removeAuthorEntity($authorEntity);
        return $isRemoved;
    }

    /**
     * Find author by id on data base
     * @param string $id
     * @return object
     */
    private function recoverAuthorEntity(string $id)
    {
        try {
            $repository = $this->entityManager->getRepository(self::AUTHOR_BUNDLE_ENTITY_AUTHOR);
            $authorEntity = $repository->find($id);
        } catch (DatabaseObjectExistsException $e) {
            echo "Error getting author " . $e;
            return null;
        }
        return $authorEntity;
    }

    /**
     * Removes author from data base
     * @param $authorEntity
     * @return bool
     */
    private function removeAuthorEntity(Author $authorEntity): bool
    {
        try {
            $this->entityManager->remove($authorEntity);
            $this->entityManager->flush();
        } catch (DatabaseObjectExistsException $e) {
            echo "Error removing author " . $e;
            return false;
        }

        return true;

    }

}