<?php


namespace AuthorBundle\Services;


use AuthorBundle\Entity\Author;
use AuthorBundle\Model\AuthorModel;
use Doctrine\DBAL\Exception\DatabaseObjectExistsException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service class to view all details from author
 * Class ViewAuthor
 * @package AuthorBundle\Services
 */
class ViewAuthor
{
    const AUTHOR_BUNDLE_ENTITY_AUTHOR = 'AuthorBundle\Entity\Author';
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var FormFactoryInterface */
    private $form;

    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $form)
    {
        $this->entityManager = $entityManager;
        $this->form = $form;
    }

    /**
     * This function recover the author by id and returns author data on array object
     * @param Request $request
     * @param string $id
     * @return array|null
     */
    public function view(Request $request, string $id){

        $authorEntity = $this->recoverAuthorEntity($id);
        if ($authorEntity !== null) {
            $authorModel = AuthorUtils::generateAuthorModel($authorEntity);
            return array('author' => $authorModel);
        }
        return null;

    }
    

    /**
     * @param string $id
     * @return Author
     */
    private function recoverAuthorEntity(string $id)
    {
        try {
            $repository = $this->entityManager->getRepository(self::AUTHOR_BUNDLE_ENTITY_AUTHOR);
            $author = $repository->find($id);
        } catch(DatabaseObjectExistsException $e) {
            echo "Error getting author:" . $e;
            return null;
        }

        return $author;
    }

}