<?php

namespace CacheBundle\EventListener;

use ArticleBundle\Event\ArticleCreatedEvent;
use ArticleBundle\Event\ArticleEditedEvent;
use ArticleBundle\Event\ArticleRemovedEvent;
use CacheBundle\Services\CachedFile;
use \Symfony\Component\EventDispatcher\Event;

class CacheArticleEventListener {


    private $articleCached;

    public function __construct(CachedFile $articleCached)
    {
        $this->articleCached = $articleCached;
    }
    
    public function cachedNewFile(ArticleCreatedEvent $event){
        $this->articleCached->cachedNewFile($event->getArticle());
    }

    public function cacheDeleteFile(ArticleRemovedEvent $event) {
        $this->articleCached->cacheDeleteFile($event->getArticle());
    }
    
    public function cacheEditedFile(ArticleEditedEvent $event) {
        $this->articleCached->cacheEditedFile($event->getArticle());
    }

    

}