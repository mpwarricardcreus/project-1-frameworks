<?php
/**
 * Created by PhpStorm.
 * User: ricard
 * Date: 5/3/17
 * Time: 18:53
 */

namespace CacheBundle\Services;

use ArticleBundle\Entity\Article;

class CachedFile
{

    /**
     * Simulate the article will be cached when executes the event
     * @param Article $article
     */
    public function cachedNewFile(Article $article)
    {
        die("Added new article content to cache: " . $article->getTitle() .
            "</br></br><a href=\"../viewarticle/" . $article->getId() . "\" target=\"_parent\">View Article</a>");
    }

    /**
     * Simulate when article has been removed from cache
     * @param Article $article
     */
    public function cacheDeleteFile(Article $article)
    {
        die("This item has been removed from the cache: " . $article->getTitle().
            "</br></br><a href=\"../listarticles" . $article->getId() . "\" target=\"_parent\">List Articles</a>");
    }

    /**
     * Simulate the updated cache from the article edited
     * @param Article $article
     */
    public function cacheEditedFile(Article $article)
    {
        die("This item has been updated from the cache: " . $article->getTitle() .
            "</br></br><a href=\"../viewarticle/" . $article->getId() . "\" target=\"_parent\">View Article</a>");
    }

}