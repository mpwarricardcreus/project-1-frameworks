<?php

namespace CommentsBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class CommentsController extends Controller
{


    /**
     * This controller shows the new form to add new comments and send the request information to the AddCommentService
     * The service validate and calls the backend functions and listeners.
     * @Route("/newcomment", name="newComment")
     * @Template
     */
    public function newAction(Request $request)
    {

        $addCommentService = $this->get('app.add_comment');
        $addComment = $addCommentService->add($request);

        if (array_key_exists('id', $addComment)){
            return $this->redirectToRoute('viewArticle',$addComment);
        }
        return $addComment;
    }


}