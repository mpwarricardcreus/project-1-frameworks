<?php

/**
 * Created by PhpStorm.
 * User: ricard
 * Date: 5/3/17
 * Time: 21:15
 */

namespace CommentsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity  */
class Comments
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $commentText;

    /**
    /**
     * @ORM\ManyToOne(targetEntity="ArticleBundle\Entity\Article", inversedBy="comments")
     * @ORM\JoinColumn(name="id_article", referencedColumnName="id")
     *
     */
    private $article;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param mixed $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }

    

    /**
     * @return mixed
     */
    public function getCommentText()
    {
        return $this->commentText;
    }

    /**
     * @param mixed $commentText
     */
    public function setCommentText($commentText)
    {
        $this->commentText = $commentText;
    }
    
    

}