<?php

namespace CommentsBundle\Event;

use CommentsBundle\Entity\Comments;
use Symfony\Component\EventDispatcher\Event;

class CommentAddedEvent extends Event{
    
    private $comment;

    public function __construct(Comments $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return Comments
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param Comments $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

   
}