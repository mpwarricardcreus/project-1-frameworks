<?php


namespace CommentsBundle\Form\Type;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;

class CommentsType extends AbstractType
{
    public function buildForm (FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', TextType::class, array(
                'required' => true,
            ))
            ->add('commentText', TextType::class, array(
                'required'  => true,
            ))
            ->add('article', EntityType::class, array(
                'class'   => 'ArticleBundle:Article',
                'choice_label' => 'title',
                'multiple' => false,
                'required' => true,
            ))
            ->add('save',SubmitType::class, array('label' => 'Create Comment'));
    }

    /**
     * The data_class type is the Model of Comments not the entity
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CommentsBundle\Model\CommentsModel'
        ));
    }

}