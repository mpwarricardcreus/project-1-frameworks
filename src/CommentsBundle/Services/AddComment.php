<?php


namespace CommentsBundle\Services;

use CommentsBundle\Entity\Comments;
use CommentsBundle\Event\CommentAddedEvent;
use CommentsBundle\Form\Type\CommentsType;
use CommentsBundle\Model\CommentsModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcher;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;


class AddComment
{

    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var FormFactoryInterface */
    private $form;
    /** @var  TraceableEventDispatcherInterface */
    private $eventDispatcher;

    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $form, TraceableEventDispatcher $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->form = $form;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * Return the empty new comment form or validate and save the new comment added 
     * @param $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function add(Request $request)
    {
        $commentModel = new CommentsModel();
        $form = $this->form->create(CommentsType::class, $commentModel);

        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $commentEntity = $this->generateNewCommentsEntity($form->getData());
            $this->saveComment($commentEntity);
            $this->generateCommentEvent($commentEntity);
            return array('id' => $commentEntity->getArticle()->getId());
        }

        return array('form' => $form->createView());
    }

    /**
     * This function generates new CommentEntity from CommentModel object
     * @param $commentsModel
     * @return Comments
     */
    private function generateNewCommentsEntity(CommentsModel $commentsModel): Comments
    {
        $commentsEntity = new Comments();
        $commentsEntity->setArticle($commentsModel->getArticle());
        $commentsEntity->setAuthor($commentsModel->getAuthor());
        $commentsEntity->setCommentText($commentsModel->getCommentText());
        $commentsEntity->setId($commentsModel->getId());

        return $commentsEntity;
    }

    
    /**
     * Save the new comment on data base
     * @param $comment
     */
    private function saveComment($comment)
    {
        $this->entityManager->persist($comment);
        $this->entityManager->flush();
    }

    /**
     * Generates a new event 'comment_added' when the comment is saved
     * @param $comment
     */
    private function generateCommentEvent($comment)
    {
        $this->eventDispatcher->dispatch('comment_added', new CommentAddedEvent($comment));
    }


}